/// <reference types="react-scripts" />

// for dispatch store
interface Todo {
  todo: string;
  isCheck: boolean;
}

interface State {
  todos: Todo[];
  Token: string;
}

interface ActionInterface {
  type: string;
  payload: any;
}

interface ParamTypes {
  id: string
}