import React from 'react'
import Username from 'src/components/InputBox/username';
import Password from 'src/components/InputBox/password';

import logo from 'src/asset/images/Logo_of_EGAT_Thailand.png'

import {
  useHistory 
} from "react-router-dom";

function Login({store}: any) {
  
  let history = useHistory();

  const [VUsername, setVUsername] = React.useState<string>(
    "",
  )
  const [VPassword, setVPassword] = React.useState<string>(
    "",
  )

  const onTextUsernameChange = (value: string) => {
    setVUsername(value)
  }

  const onTextPasswordChange = (value: string) => {
    setVPassword(value)
  }

  const GotoDashboard = () => {
    history.push({
      pathname: `/dashboard/controlpanal`
    });
  }

  return (
    <div>
      <div className="loginContainer-image"></div>
      <div className="loginContainer">
        <div className="loginBox"> 
          <div className="banner-egat-imgs">
            <img src={logo} alt="logo" width="100px" height="120px" style={{opacity: 10, zIndex: 20000}}/>
          </div>
          <div className="banner-egat-main" style={{opacity: 10, color: "#fff"}}>การไฟฟ้าฝ่ายผลิตแห่งประเทศไทย</div>
          <div className="banner-egat-sub" style={{opacity: 10, color: "#fff"}}>Electricity Generating Authority of thanland</div>
          <div className="InputUsername">
            <Username value={VUsername} onTextChange={onTextUsernameChange}/>
          </div>
          
          <div className="InputPassword">
            <Password  value={VPassword} onTextChange={onTextPasswordChange} onIconBtnClick={GotoDashboard}/>
          </div>
        </div>
      </div>
      
    </div>
  );
}

export default Login;
