import React from 'react'
import {
  Switch,
  Route,
  useRouteMatch
} from "react-router-dom";

import ControlPanal from 'src/pages/Dashboard/ControlPanal';
import Device from 'src/pages/Dashboard/Device';
import Report from 'src/pages/Dashboard/Report';
import Manage from 'src/pages/Dashboard/Manage';

import DashboardNavbar from 'src/pages/Dashboard/DashboardNavbar'
import DashboardSidebar from 'src/pages/Dashboard/DashboardSidebar';

import 'src/scss/style.main.scss'

function DashboardLayout() {
  let { path } = useRouteMatch();
  return (
    <div className="LayoutContainer">
      <DashboardSidebar />
      <div className="LayoutContainerFlexColume">
        <DashboardNavbar />
        <div className="LineDivide"></div>
        <div id="layout-container-scroll" className="OutleContainer">
          <Switch>
            <Route path={`${path}/controlpanal`}>
              <ControlPanal />
            </Route>
            <Route path={`${path}/device`}>
              <Device />
            </Route>
            <Route path={`${path}/report`}>
              <Report />
            </Route>
            <Route path={`${path}/manage`}>
              <Manage />
            </Route>
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default DashboardLayout;
