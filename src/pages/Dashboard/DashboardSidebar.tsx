import React from 'react'
import {
  Link,
  useLocation
} from "react-router-dom";
import styled from 'styled-components';

// images
import Logo from 'src/asset/images/DashboardSidebar/Egat-logo.png'

import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import DevicesOtherIcon from '@material-ui/icons/DevicesOther';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';

import clsx from 'clsx'

const StyledLink = styled(Link)`
    text-decoration: none;
    color: inherit;
    &:focus, &:hover, &:visited, &:link, &:active, &:click {
        text-decoration: none;
        color: inherit;
    }
`;

function DashboardSidebar() {
  const location = useLocation();

  const ActiveMenu = (path: String, pathMenu: String) => {
    if (path === pathMenu){
      return clsx("item-box", "menu-active")
    }else if (path === pathMenu) {
      return clsx("item-box", "menu-active")
    }else if (path === pathMenu) {
      return clsx("item-box", "menu-active")
    }else if (path === pathMenu) {
      return clsx("item-box", "menu-active")
    }else {
      return clsx("item-box")
    }
  }

  return (
    <div className="SidebarContainer">

      <div className="LogoHeader">
        <img src={Logo} alt={"Logo"} />
        <div className="LogoHeader-title">
          การไฟฟ้าฝ่ายผลิตแห่งประเทศไทย
          <div className="LogoHeader-subtitle">Electricity Generating Authority of Thailand</div>
        </div>
      </div>

      <div className="LineDivide"></div>

      <StyledLink to="/dashboard/controlpanal">
        <ul className={ActiveMenu(location.pathname, "/dashboard/controlpanal")}>
          <li>
            <div className="item-menu">
              <DashboardOutlinedIcon color="primary" /><span className="text-menu">แผงควบคุม</span>
            </div>
          </li>
        </ul>
      </StyledLink>


      <StyledLink to="/dashboard/device">
        <ul className={ActiveMenu(location.pathname, "/dashboard/device")}>
          <li>
            <div className="item-menu">
              <DevicesOtherIcon color="primary" /><span className="text-menu">อุปกรณ์</span>
            </div>
          </li>
        </ul>
      </StyledLink>



      <StyledLink to="/dashboard/report">
        <ul className={ActiveMenu(location.pathname, "/dashboard/report")}>
          <li>
            <div className="item-menu">
              <AssessmentOutlinedIcon color="primary" /><span className="text-menu">รายงาน</span>
            </div>
          </li>
        </ul>
      </StyledLink>



      <StyledLink to="/dashboard/manage">
        <ul className={ActiveMenu(location.pathname, "/dashboard/manage")}>
          <li>
            <div className="item-menu">
              <EditOutlinedIcon color="primary" /><span className="text-menu">จัดการ</span>
            </div>
          </li>
        </ul>
      </StyledLink>

    </div>
  );
}

export default DashboardSidebar;
