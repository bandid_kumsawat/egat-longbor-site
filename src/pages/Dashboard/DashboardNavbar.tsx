import React from 'react'

import DateTime from 'src/components/DateTime'
import UserIcon from 'src/components/UserIcon'

function DashboardNavbar() {
  return (
    <div className="NavbarContainer">
      <div className="DateTime">
        <DateTime></DateTime>
      </div>
      <div className="LogoUser">
        <UserIcon ></UserIcon>
      </div>
    </div>
  );
}

export default DashboardNavbar;
