import React from 'react'
import BoxShowValue from 'src/components/BoxShowValue'
import MapDevice from 'src/components/MapDevice'
import TableSummary from 'src/components/TableSummary'
import Record from 'src/components/Record'

// image
import carIn from 'src/asset/images/Icon/car-in.png'
import carOut from 'src/asset/images/Icon/car-out.png'
import peopleIn from 'src/asset/images/Icon/people-in.png'
import peopleOut from 'src/asset/images/Icon/people-out.png'

import Grid from '@material-ui/core/Grid';

function ControlPanal() {

  const [Value, setValue] = React.useState(0)

  React.useEffect(() => {
    var interval_ = setInterval(() => {
      setValue(Math.random() * 3000)
    }, 2000);
    return () => {
      clearInterval(interval_)
    }
  }, [])

  return (
    <div>
      <div className="rowFirstBoxshowvalue">
      <Grid item xs={6} sm={3} style={{marginRight: "15px"}} >
        <BoxShowValue
          props={
            [
              {
                value: Value,
                Title: "จำนวนคนเข้าทั้งหมด"
              }
            ]  
          }  
          child={<img src={peopleIn} alt="peopleIn" width="30px" height="30px"/>}
          callback={() => {
            return Value
          }}
        />
      </Grid>
      <Grid item xs={6} sm={3} style={{marginRight: "15px"}} >
      <BoxShowValue
          props={
            [
              {
                value: Value,
                Title: "จำนวนคนออกทั้งหมด"
              }
            ]  
          }  
          child={<img src={peopleOut} alt="peopleIn" width="30px" height="30px"/>}
          callback={() => {
            return Value
          }}
        />
      </Grid>
      <Grid item xs={6} sm={3} style={{marginRight: "15px"}} >
      <BoxShowValue
          props={
            [
              {
                value: Value,
                Title: "จำนวนรถเข้าทั้งหมด"
              }
            ]  
          }  
          child={<img src={carIn} alt="peopleIn" width="40px" height="30px"/>}
          callback={() => {
            return Value
          }}
        />
      </Grid>
      <Grid item xs={6} sm={3} >
      <BoxShowValue
          props={
            [
              {
                value: Value,
                Title: "จำนวนรถออกทั้งหมด"
              }
            ]  
          }  
          child={<img src={carOut} alt="peopleIn" width="40px" height="30px"/>}
          callback={() => {
            return Value
          }}
        />
      </Grid>
      </div>
      <div className="rowSecondMapshowdevice">
        <Grid item xs={12} sm={6} style={{marginRight: "15px"}}>
          <MapDevice />
        </Grid>
        <Grid item xs={12} sm={6} >
          <TableSummary />
        </Grid>
      </div>


      <div className="rowThirdRecord">
        <Grid item xs={12} sm={12}>
          <Record />
        </Grid>
      </div>

    </div>
  );
}

export default ControlPanal;
