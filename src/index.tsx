import React from 'react';
import ReactDOM from 'react-dom';
import 'src/index.scss';
import App from 'src/App';
import { HashRouter } from "react-router-dom";

import configureStore from "src/stores";
const store = configureStore();

ReactDOM.render(
  <HashRouter>
    <App store={store}/>
  </HashRouter>,
  document.getElementById('root')
);
