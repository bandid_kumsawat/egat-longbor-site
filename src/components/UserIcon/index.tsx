import React from 'react'
import { createStyles, withStyles, Theme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';

// image
import User from 'src/asset/images/User/user.png'

const ButtonIcon = withStyles((theme: Theme) =>
  createStyles({
    root: {

    },
  }),
)(IconButton);

function UserIcon() {

  return (
    <ButtonIcon>
      <img src={User} alt={User} width={40} height={40}/>
    </ButtonIcon>
  );
}

export default UserIcon;
