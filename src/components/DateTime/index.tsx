import React from 'react'
import {
  createStyles,
  fade,
  Theme,
  withStyles,
} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase'
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';

const TextBoxInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.common.white,
      border: '1px solid #ced4da',
      fontSize: 16,
      width: 'auto',
      padding: '10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        'Kanit',
      ].join(','),
      '&:focus': {
        boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
        borderColor: theme.palette.primary.main,
      },
    },
  }),
)(InputBase);


function DateTime() {

  const [selectedDate, setSelectedDate] = React.useState<string | null>(
    "2021-06-12",
  )

  const handleDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedDate(event.target.value)
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
       <TextBoxInput type="date" value={selectedDate} onChange={handleDateChange} id="bootstrap-input" />
    </MuiPickersUtilsProvider>
  );
}

export default DateTime;
