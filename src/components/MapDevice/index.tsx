import React from 'react'

// image
import MapDeviceImage from 'src/asset/images/google_map.png'

function MapDevice () {
  return <div className="main-box-map">
    <header className="headerMap">
      แผนที่
    </header>
    <div className="LineDivide"></div>
    <div className="containerMap">
      <img src={MapDeviceImage} alt="MapDeviceImage" width="100%" height="600px"/>
    </div>
  </div>
}
export default MapDevice