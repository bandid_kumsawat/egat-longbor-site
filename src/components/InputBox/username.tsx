import React from 'react'
import {
  withStyles,
} from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput'

const TextBoxInput = withStyles({
  root: {
    width: "100%",
    height: "50px",
    backgroundColor: "#fff",
    '&:hover fieldset': {
      borderColor: '#0739A2cd',
    },
  },
})(OutlinedInput);

interface Props {
  value: string;
  onTextChange: (value: string) => void;
}

function Username({value, onTextChange}: Props) {

  const [InputValue, setInputValue] = React.useState<string | null>(
    String(value),
  )

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    var value_: string = event.target.value
    setInputValue(event.target.value)
    onTextChange(value_)
  }

  return (
    <TextBoxInput
      type="text" 
      value={InputValue} 
      onChange={handleUsernameChange} 
      id="username-auth-input" 
      autoComplete={"off"}
      placeholder="Username"
    />
  );
}

export default Username;
