import React from 'react'
import {
  withStyles,
} from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { IconButton } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';

const TextBoxInput = withStyles({
  root: {
    width: "100%",
    height: "50px",
    backgroundColor: "#fff",
    '&:hover fieldset': {
      borderColor: '#0739A2cd',
    },
  },
})(OutlinedInput);

interface Props {
  value: string;
  onTextChange: (value: string) => void;
  onIconBtnClick: () => void;
}

function Password({value, onTextChange, onIconBtnClick}: Props) {

  const [InputValue, setInputValue] = React.useState<string | null>(
    String(value),
  )

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    var value_: string = event.target.value
    setInputValue(event.target.value)
    onTextChange(value_)
  }

  return (
    <TextBoxInput
      id="outlined-adornment-password"
      type={"password"}
      onChange={handlePasswordChange}
      placeholder="Password"
      value={InputValue}
      endAdornment={
        <InputAdornment position="end">
        <IconButton
          edge="end"
          onClick={() => {
            onIconBtnClick()
          }}
        >
          <ArrowForwardIosIcon />
        </IconButton>
        </InputAdornment>
      }
    />
  );
}

export default Password;