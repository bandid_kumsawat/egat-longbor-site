import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const rows = [
  {
    name: "ด่านลบบ่อ 1",
    pin: 10,
    pout: 29,
    cin: 92,
    cout: 292
  },  {
    name: "ด่านลงบ่อ 2",
    pin: 10,
    pout: 29,
    cin: 92,
    cout: 292
  },  {
    name: "ด่านลงบ่อ 3",
    pin: 10,
    pout: 29,
    cin: 92,
    cout: 292
  },  {
    name: "ด่านลงบ่อ แนวสายพาน",
    pin: 10,
    pout: 29,
    cin: 92,
    cout: 292
  },{
    name: "รวม",
    pin: 100,
    pout: 290,
    cin: 920,
    cout: 2920
  }
];

function TableSummary () {

  const classes = useStyles();

  return <div className="main-box-table">
    <header className="headertable">
      ตารางสรุปจำนวนเข้า-ออก
    </header>
    <div className="LineDivide"></div>
    <div className="containertable">
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{fontSize: "18px"}} >ด่าน</TableCell>
              <TableCell style={{fontSize: "18px"}} align="right">คนเข้า</TableCell>
              <TableCell style={{fontSize: "18px"}} align="right">คนออก</TableCell>
              <TableCell style={{fontSize: "18px"}} align="right">รถเข้า</TableCell>
              <TableCell style={{fontSize: "18px"}} align="right">รถออก</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell scope="row">
                  {row.name}
                </TableCell>
                <TableCell style={{height: "50px"}} align="right">{row.pin}</TableCell>
                <TableCell align="right">{row.pout}</TableCell>
                <TableCell align="right">{row.cin}</TableCell>
                <TableCell align="right">{row.cout}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  </div>
}
export default TableSummary