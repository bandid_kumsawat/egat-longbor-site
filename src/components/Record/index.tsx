import React from 'react';
import { makeStyles, withStyles, Theme, createStyles,  } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box'

import TablePeople from 'src/components/TablePeople'
import TableCar from 'src/components/TableCar'

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
  },
  padding: {
    padding: theme.spacing(3),
  },
  demo1: {
    backgroundColor: theme.palette.background.paper,
  },
  demo2: {
    backgroundColor: '#2e1534',
  },
}));

const AntTabs = withStyles({
  root: {
    borderBottom: '1px solid #e8e8e8',
  },
  indicator: {
    backgroundColor: '#1890ff',
  },
})(Tabs);

interface StyledTabProps {
  label: string;
}

const AntTab = withStyles((theme: Theme) =>
  createStyles({
    root: {
      textTransform: 'none',
      minWidth: 72,
      fontWeight: theme.typography.fontWeightRegular,
      marginRight: theme.spacing(4),
      fontFamily: [
        'kanit',
      ].join(','),
      '&:hover': {
        color: '#223296',
        opacity: 1,
      },
      // '&:selected': {
      //   color: '#223296',
      //   fontWeight: theme.typography.fontWeightMedium,
      // },
      '&:focus': {
        color: '#223296',
      },
    },
    selected: {},
  }),
)((props: StyledTabProps) => <Tab disableRipple {...props} />);


function Record () {

  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    console.log(newValue)
    setValue(newValue);
  };

  return <div className="main-box-record">
    {/* <div className="LineDivide"></div> */}
    <div className="containerRecord">
      <div className={classes.root}>
        <div className={classes.demo1}>
          <AntTabs value={value} onChange={handleChange} aria-label="ant example">
            <AntTab label="รายการคนเข้า-ออก" {...a11yProps(0)}/>
            <AntTab label="รายการทะเบียนรถเข้า-ออก" {...a11yProps(1)}/>
          </AntTabs>
          
          
          <TabPanel value={value} index={0}>
            <TablePeople />
          </TabPanel>

          <TabPanel value={value} index={1}>
            <TableCar />
          </TabPanel>

        </div>
      </div>
    </div>
  </div>
}
export default Record