

import React from 'react'

interface Props {
  props: { value: number, Title: string}[];
  child: JSX.Element;
  callback: () => number;
}

const BoxShowValue = ({props, child, callback}: Props) => {

  // const [Data] = React.useState(props[0].value)
  const [Title] = React.useState(props[0].Title)

  return (
    <div className="main-box">
      <div className="icon-people-box">
        {child}
      </div>
      <div>
        <div>{Title}</div>
        <div>{callback().toFixed(2)}</div>
      </div>
    </div>
  );
}

export default BoxShowValue;