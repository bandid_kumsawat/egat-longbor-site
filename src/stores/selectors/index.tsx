import { RootState } from "src/stores";

export function getTodoState(state: RootState) {
	return state.todoReducer.todos;
}
