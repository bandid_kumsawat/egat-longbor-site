import { createStore, combineReducers } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';

import { todoReducer } from "./reducers";

const rootReducer = combineReducers({todoReducer});
export type RootState = ReturnType<typeof rootReducer>;

export default function configureStore() {
	const store = createStore(todoReducer, composeWithDevTools());
	return store;
}
