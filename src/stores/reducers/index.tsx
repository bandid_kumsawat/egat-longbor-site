import state from 'src/stores/reducers/state'

const initialState: State = state

export function todoReducer(state = initialState, action: ActionInterface): State {
	switch (action.type) {
		case "ADD_TODO":
			state.todos.push(action.payload)
			return Object.assign({}, state, {
        todos: state.todos
      })
		case "ADD_TOKEN":
			return Object.assign({}, state, {
				Token: action.payload
			})
		default:
			return state;
	}
}
