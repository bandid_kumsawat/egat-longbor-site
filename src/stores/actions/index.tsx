export function addTodo(todo: Todo): ActionInterface {
	return {
		type: "ADD_TODO",
		payload: todo,
	};
}

export function addToken(Token: string): ActionInterface {
	return {
		type: "ADD_TOKEN",
		payload: Token,
	};
}
