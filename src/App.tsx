import React from 'react';
import './App.scss';
import styled from 'styled-components'
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import DashboardLayout from 'src/pages/Dashboard'
import Login from 'src/pages/Login'

const AppContainer = styled('div')`
  display: block;
`

const theme = createMuiTheme({
  breakpoints: {
    values: {
      // xs: 0,
      // sm: 450,
      // md: 600,
      // lg: 900,
      // xl: 1200
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1600
    }
  },
  palette: {
    primary: {
      main: "#0739A2",
    },
    secondary: {
      main: "#E5E5E5",
    },
  },
});

function App({store}: any) {

  React.useEffect(() => {
    // console.log(store.getState().todos)
  }, [store])

  return (
    <MuiThemeProvider theme={theme}>
      <AppContainer className="App">
        <Switch>
          <Route path="/" exact>
            <Redirect to="/login" />
          </Route>
          <Route path="/dashboard">
            <DashboardLayout />
          </Route>
          <Route path="/login">
            <Login store={store}/>
          </Route>
        </Switch>
      </AppContainer>
    </MuiThemeProvider>
  );
}

export default App;
