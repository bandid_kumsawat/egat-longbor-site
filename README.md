### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:22221](http://localhost:22221) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

### `react-app-env.d.ts`

file setup Environment static type variable.